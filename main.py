from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_restful import Api, Resource

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)
ma = Marshmallow(app)
api = Api(app)

class Room(db.Model):
    __tablename__ = "rooms"
    room_id = db.Column(db.Integer, primary_key = True)
    room_type = db.Column(db.String(40))
    room_descrip = db.Column(db.String(40))
    room_price = db.Column(db.String(40))
    bookings = db.relationship("Booking", cascade="all, delete-orphan")

    def __repr__(self):
        return '<Room %d>' % self.room_id

class Booking(db.Model):
    __tablename__ = "bookings"
    booking_num = db.Column(db.Integer, primary_key = True)
    room_id = db.Column(db.Integer, db.ForeignKey('rooms.room_id'))
    #room_id = db.Column(db.Integer)
    check_in = db.Column(db.String(40))
    check_out = db.Column(db.String(40))
    guest_name = db.Column(db.String(40))

    def __repr__(self):
        return '<Booking %d>' % self.booking_num

class RoomSchema(ma.Schema):
    class Meta:
        fields = ("room_id", "room_type", "room_descrip", "room_price")
        model = Room

room_schema = RoomSchema()
rooms_schema = RoomSchema(many=True)

class BookingSchema(ma.Schema):
    class Meta:
        fields = ("booking_num", "room_id", "check_in", "check_out", "guest_name")
        model = Booking

booking_schema = BookingSchema()
bookings_schema = BookingSchema(many=True)

class RoomListResource(Resource):
    def get(self):
        rooms = Room.query.all()
        return rooms_schema.dump(rooms)

    def post(self):
        new_room = Room(
                room_id = request.json['room_id'],
                room_type = request.json['room_type'],
                room_descrip = request.json['room_descrip'],
                room_price = request.json['room_price']
                )
        db.session.add(new_room)
        db.session.commit()
        return room_schema.dump(new_room)

class BookingListResource(Resource):
    def get(self):
        check_in = request.json['check_in']
        check_out=request.json['check_out']

        if (check_in > check_out):
            return "ERROR: INVALID DATES"

        av_rooms = Room.query.join(Booking, Booking.booking_num == Room.room_id, isouter = True).filter(db.or_(Booking.booking_num==None, \
                db.or_(db.and_(check_in<Booking.check_in, check_out<Booking.check_in), db.and_(check_in>Booking.check_out, check_out>Booking.check_out)))).all()


        return rooms_schema.dump(av_rooms)

    def post(self):
        new_booking = Booking(room_id = request.json['room_id'],
                check_in = request.json['check_in'],
                check_out = request.json['check_out'],
                guest_name = request.json['guest_name']
                )
        db.session.add(new_booking)
        db.session.commit()
        return booking_schema.dump(new_booking)


class RoomResource(Resource):
    def get(self, room_id):
        room = Room.query.get_or_404(room_id)
        #return room_schema.dump(room)
        return "$" + room.room_price

    def patch(self, room_id):
        room = Room.query.get_or_404(room_id)

        if 'room_type' in request.json:
            room.room_type = request.json['room_type']

        if 'room_descrip' in request.json:
            room.room_descrip = request.json['room_descrip']

        if 'room_price' in request.json:
            room.room_price = request.json['room_price']

        db.session.commit()
        return room_schema.dump(room)

    def delete(self, room_id):
        room = Room.query.get_or_404(room_id)
        db.session.delete(room)
        db.session.commit()
        return '', 204


class BookingResource(Resource):
    def get(self, booking_num):
        booking = Booking.query.get_or_404(booking_num)
        return booking_schema.dump(booking)

    def patch(self, booking_num):
        booking = Booking.query.get_or_404(booking_num)

        if 'room_id' in request.json:
            booking.room_id = request.json['room_id']
        if 'check_in' in request.json:
            booking.check_in = request.json['check_in']
        if 'check_out' in request.json:
            booking.check_out = request.json['check_out']
        if 'guest_name' in request.json:
            booking.guest_name = request.json['guest_name']

        db.session.commit()
        return booking_schema.dump(booking)

    def delete(self, booking_num):
        booking = Booking.query.get_or_404(booking_num)
        db.session.delete(booking)
        db.session.commit()
        return '', 204


api.add_resource(RoomListResource, '/rooms')
api.add_resource(BookingListResource, '/bookings')
api.add_resource(RoomResource, '/rooms/<int:room_id>')
api.add_resource(BookingResource, '/bookings/<int:booking_num>')

if __name__ == '__main__':
    app.run(debug=True)
